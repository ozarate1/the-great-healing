

export default class Effect {
  constructor(name) {
    this.name = name;
    
  }

  
  static from(name) {
    return new Effect(name)
  }
}
