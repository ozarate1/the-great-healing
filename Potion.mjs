import {debuffs as debuffData} from "./data.mjs";

export default class Potion {
    constructor(name, value, weight, time, attributes) {
        this.name = name;
        this.value = value;
        this.weight = weight;
        this.time = time; //TODO: No lo utilizar
        this.attributes = attributes;
    }

    static with(diseases, weight, value) {

        //Extraemos el nombre de la poción
        const diseaseNames = diseases.map(disease => disease.name);
        const potionName = `Potion of healing ${diseaseNames.toString()}`;

        //Extraemos los atributos asociados a los diferentes efectos y les cambiamos el signo
        const potionEffects = Potion.getEffectDataWithOppositeValue(diseases);
        console.log(potionEffects);


        
        const time = 10; //No se usa de momento
        return new Potion(potionName, value, weight, time, potionEffects);
    }

    
    static failed() {
        return new FailedPotion();
    }


    static getEffectDataWithOppositeValue(diseases)
    {
        let totalEffects = [];
        let attributeNames = [];

        //console.log("Entra");
        diseases.forEach( disease => disease.getEffectDataWithOppositeValue(totalEffects, attributeNames));

        return totalEffects;

    }

}

class FailedPotion extends Potion {
  constructor() {
    super("Failed potion", 0, 0, 0);
  }
}


