const herbs = [
        {
            "effects": [
                "Remove strength damage",
                "Remove attack damage"
            ],
                
            "image": "http://www.uesp.net/w/images/thumb/9/9d/SR-icon-herb-Wisp_Wrappings.png/48px-SR-icon-herb-Wisp_Wrappings.png",
            "value": 2,
            "name": "White blossom",
            "weight": 0.1
        },
        {
            "effects": [
                "Remove HP damage",
                "Restore fumble damage"
            ],
                
            "image": "http://www.uesp.net/w/images/thumb/9/9d/SR-icon-herb-Wisp_Wrappings.png/48px-SR-icon-herb-Wisp_Wrappings.png",
            "value": 3,
            "name": "Crimson Willow",
            "weight": 0.1
        },
        {
            "effects": [
                "Remove intelligence damage",
                "Restore fumble damage",
                "Remove HP damage"

            ],
            "image": "http://www.uesp.net/w/images/thumb/9/9d/SR-icon-herb-Wisp_Wrappings.png/48px-SR-icon-herb-Wisp_Wrappings.png",
            "value": 30,
            "name": "Ebony rose",
            "weight": 0.1
        }

]





const debuffs = {
    "Damage attack":        { attack: -20 },
    "Lessen intelligence":  { intelligence_time: 1},
    "Damage intelligence":  { intelligence: -20},
    "Lessen HP":            { hit_points_time: -1},
    "Damage HP":            { hit_points: -20},
    "Increase fumble":      { damage_fumble: +50 },
    "Decrease speed":       { speed: -20 }
}


const diseases = 
[
    {       
        name: "Psychosis",
        image: null,
        effects:[
            "Damage attack",
            "Lessen intelligence",           
        ]
            
    },
    {       
        name: "Madness",
        image: null,
        effects:[
            "Damage attack",
            "Increase fumble"
        ]
    },
    {       
        name: "Decrepitude",
        image: null,
        effects:[
            "Lessen HP",
            "Decrease speed"
        ]
    },
    {       
        name: "Feebleness",
        image: null,
        effects:[
            "Damage HP"
        ]
    }

]


export {debuffs, diseases, herbs};