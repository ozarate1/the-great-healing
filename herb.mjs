import Effect from './effect.mjs';

    export default class Herb {
    constructor(name, effects, value, weight) {
        this.name = name;
        this.effects = effects;
        this.value = value;
        this.weight = weight;

        

    }

    static from({name, effects, value, weight}) {
        return new Herb(
        name,
        effects.map(effect => Effect.from(effect)),
        value,
        weight
        );
    }

}

