import fs from 'fs';
import Debuff from './debuff.mjs';

export default class Debuffs {
    constructor(debuffs) {
        this.debuffs = debuffs;
    }

    static from(file) {
        const debuffs = JSON.parse(fs.readFileSync(file)).map(element => Debuff.from(element));
        return new Debuffs(debuffs);
    }

    find(id) {
        return this.debuffs.filter(debuff => debuff.id === id)[0];
    }
}