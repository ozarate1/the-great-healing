export default class Disease {
    constructor(name, id, image, effects) {
        this.name = name;
        this.id = id;
        this.image = image;
        this.effects = effects;
    }

    static from(data, debuffRepo) 
    {
        const effects = debuffRepo.debuffs.filter(debuff => debuff.isAvailableInDisease(data));
                
        if (effects.length != data.effects.length) 
            throw new Error (`Error creating Disease: [${data.id}]. Effects not found in the Debuff DataBase`);
        
        return new Disease(data.name, data.id, data.image, effects);
        
    }

    getEffectDataWithOppositeValue(totalEffects, attributeNames) 
    {
        //console.log("entra");
        this.effects.forEach(debuff => debuff.getEffectDataWithOppositeValue(totalEffects, attributeNames));
    }

    getDebuffs()
    {
        return this.effects;
    }

    


}

