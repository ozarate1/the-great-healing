import Potion from './Potion.mjs';

export default class Cauldron {
    
    constructor(herbsRepo, diseasesRepo) {
        this.herbs      = herbsRepo;
        this.diseases   = diseasesRepo;
    }

    createPotion(herbName1, herbName2) {

        //Extraemos los códigos de 2 ingredientes
        const herb1 = this.herbs.find(herbName1);
        const herb2 = this.herbs.find(herbName2);
        
        const herb1EffectNames = herb1.effects.map(element => element.name);
        const herb2EffectNames = herb2.effects.map(element => element.name);
        
        //Extraemos los nombres. El usar Set es opcional. No importa que se repitan.
        const effectNames = [...herb1EffectNames, ...herb2EffectNames]

        //Buscamos las enfermedades a curar
        const diseasesToHeal = this.findDiseasesInEffects(effectNames);

        if (diseasesToHeal.length > 0) 
        {
            //Existen enfermedades a curar
            return Potion.with(diseasesToHeal, 
                herb1.weight + herb2.weight,
                herb1.value + herb2.value);
        }
        else
        {
            //No existen enfermedades a curar, poción fallida
            return Potion.failed();
        }

    }


    findDiseasesInEffects(effectList)
    {
        const diseasesFound = [];

        this.diseases.diseases.forEach(disease => {    

            // Extraemos los efec en juego para la enfermedad considerada
            const diseaseDebuffs = disease.getDebuffs();

            const debuffsFoundInInputEffects = diseaseDebuffs.filter( debuff => 
                debuff.isFoundInEffectList(effectList)
            );

            if (debuffsFoundInInputEffects.length === diseaseDebuffs.length)
            {
                diseasesFound.push(disease);
            }
        
        });

        return diseasesFound;

    }

}



