import Disease from "./disease.mjs";
import fs from 'fs';


export default class Diseases {

    constructor(diseases) {
        this.diseases = diseases;
    }

    static from(file, debuffs) {
        const diseases = JSON.parse(fs.readFileSync(file)).map(element => Disease.from(element, debuffs));
        return new Diseases(diseases);
    }
}



// export function createDiseases(debuffData, diseaseData)
// {
//     //Creamos array de Debuffs
//     const debuffs = [];
//     for (let debuff in debuffData)
//     {
//         const data = Object.assign({}, {name: debuff}, debuffData[debuff]);
//         debuffs.push(Debuff.from(data))
//     }

//     //Creamos array de Diseases
//     const diseases = diseaseData.map(element => Disease.from(element, debuffs));

//     return diseases;
// }


