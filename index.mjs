import Debuffs from "./debuffs.mjs";
import Herbs from "./herbs.mjs";
import Diseases from "./diseases.mjs";
import Cauldron from "./cauldron.mjs";

// Inicializar repositorios

const debuffsRepo   = Debuffs.from("data/debuffs.json");
const diseasesRepo  = Diseases.from("data/diseases.json", debuffsRepo);
const herbsRepo     = Herbs.from("data/herbs.json");

// console.log(b.length);
// debuffsRepo.debuffs.forEach(element => {
//   console.log(element);
// });

// diseasesRepo.diseases.forEach(element => {
//     console.log(element);
// });

// herbsRepo.herbs.forEach(element => {
//     console.log(element);
// });

// //Creamos el calderon de pociones
const cauldron = new Cauldron(herbsRepo, diseasesRepo);

// // // // //Creamos pociones
const potion1 = cauldron.createPotion("Ebony rose", "White blossom");
showPotion(potion1);


function showPotion(potion) {
  console.log(`${potion.name}`);
  console.log(`Value:         ${potion.value}`);
  console.log(`Weight:        ${potion.weight}`);
  console.log(`Time:          ${potion.time}`);

  console.log(`Attributes: `);
  console.log(potion.attributes);
  

  
}











