import fs from 'fs';
import Herb from './herb.mjs';

export default class Herbs {
    constructor(herbs) {
        this.herbs = herbs;
    }

    static from(file) {
        const herbs = JSON.parse(fs.readFileSync(file)).map(element => Herb.from(element));
        return new Herbs(herbs);
        
    }


    find(name)
    {
        return this.herbs.find(herb => herb.name === name);
    }
}