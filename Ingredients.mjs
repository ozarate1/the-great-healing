import Herb from "./Herb.mjs";

export default class Herbs {
  constructor(herbs) {
    this.herbs = herbs;
  }

  static load(data) {
    return new Herbs(data.map(Herb.from));
  }

  find(name) {
    const herb = this.herbs.find(element => element.hasName(name));
    if (herb === undefined)
      throw new Error(`Unknown herb ${name}`);

    return herb;
  }
}
