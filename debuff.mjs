
export default class Debuff
{
    constructor() {}

    static from(data)
    {
        switch(data.type)
        {
            case "Instant":
                return InstantDebuff.from(data);
            case "Temporary":
                return TemporaryDebuff.from(data);
            default:
                throw new Error (`Unknown Type of Debuff`);
                    
        }

        
    }

    isAvailableInDisease(diseaseData)
    {
        return diseaseData.effects.some(effect => effect === this.id);
    }

    isFoundInEffectList(effectList)
    {
        return effectList.some(effect => effect === this.id);
    }
    
}


class InstantDebuff extends Debuff
{  
    constructor(name, id, attribute, value)
    {
        super();

        this.name   = name;
        this.id   = id;
        this.attribute = attribute;
        this.value = value;
        
    }

    static from(data)
    {
        return new InstantDebuff(data.name, data.id, data.attribute, data.value);   
    }

    getEffectDataWithOppositeValue(totalEffects, attributeNames)
    {
        //console.log("entra");

        if (!attributeNames.includes(this.attribute))
        {
            
            const effect = {
                attribute: this.attribute,
                value: -this.value,
                
            }

            totalEffects.push(effect);
            attributeNames.push(this.attribute);

        }

    }
        
}


class TemporaryDebuff extends Debuff
{  
    constructor(name, id, attribute, value, milliseconds_to_change, change_value)
    {
        super();

        this.name                       = name;
        this.id                         = id;
        this.attribute                  = attribute;
        this.value                      = value;
        this.milliseconds_to_change     = milliseconds_to_change;
        this.change_value               = change_value;
        
    }

    static from(data)
    {
        return new TemporaryDebuff(data.name, data.id, data.attribute, data.value, data.milliseconds_to_change, data.change_value);
    }

    getEffectDataWithOppositeValue(totalEffects, attributeNames)
    {
          if (!attributeNames.includes(this.attribute))
        {
            const effect = {
                attribute: this.attribute,
                value: -this.value,
                milliseconds_to_change: this.milliseconds_to_change,
                change_value: -this.change_value
            }

            //Añadimos a potionEffects el efecto actual 
            totalEffects.push(effect);

            attributeNames.push(this.attribute);

        }

    }
}
